import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//a class that seems thread-safe
class books {
	final Map<Integer, String> map =
		new ConcurrentHashMap<>();
	int add(String title) {
		final Integer next = this.map.size() + 1;
		this.map.put(next, title);
		return next;
	}
	String title(int id) {
		return this.map.get(id);
	}
}
