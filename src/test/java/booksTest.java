import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


import static org.hamcrest.Matchers.greaterThan; //use this to control and monitor threads' execution
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class booksTest {
	@Test
	public void concurrentModificationTest() throws ExecutionException, InterruptedException {
		books books = new books();
		int threads = 1000;
		ExecutorService service =
			Executors.newFixedThreadPool(threads);
		CountDownLatch latch = new CountDownLatch(1);
		AtomicBoolean running = new AtomicBoolean();
		AtomicInteger overlaps = new AtomicInteger();
		Collection<Future<Integer>> futures = new ArrayList<>(threads);
		for (int t = 0; t < threads; ++t) {
			final String title = String.format("Book #%d", t);
			futures.add(
				service.submit(
					() -> {
						latch.await();
						if (running.get()) {
							overlaps.incrementAndGet();
						}
						running.set(true);
						int id = books.add(title);
						running.set(false);
						return id;
					}
				)
			);
		}
		latch.countDown();
		Set<Integer> ids = new HashSet<>();
		for (Future<Integer> f : futures) {
			ids.add(f.get());
		}
		assertThat(overlaps.get(), greaterThan(0));
		assertEquals(ids.size(), threads);
	}
}
