package org.springframework.samples.petclinic.owner;



import org.junit.After;
import org.junit.Before;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.samples.petclinic.visit.Visit;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assume.assumeTrue;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(Enclosed.class) //so that we can separate part1 tests from part2 tests by putting them in different classes
public class PetTest {


	@RunWith(Theories.class)
	public static class PartTwoTheorizedTest{
		private Visit visit1 = new Visit();
		private Visit visit2 = new Visit();
		private Visit visit3 = new Visit();
		private Visit visit4 = new Visit();
		private Visit visit5 = new Visit();
		private Visit visit6 = new Visit();
		private Visit visit7 = new Visit();
		private Visit visit8 = new Visit();
		ArrayList<Visit> visits = new ArrayList<>(Arrays.asList(visit1,visit2,visit3,visit4,visit5,visit6,visit7,visit8));
		private Pet pet = new Pet();

		@Before
		public void setup(){
			LocalDate date1 = LocalDate.of(2002, 9, 11);
			LocalDate date2 = LocalDate.of(2019, 12, 23);
			LocalDate date3 = LocalDate.of(2020, 5, 4);
			LocalDate date4 = LocalDate.of(2003, 7, 30);
			LocalDate date5 = LocalDate.of(2007, 3, 17);
			LocalDate date6 = LocalDate.of(2013, 2, 16);
			LocalDate date7 = LocalDate.of(2008, 1, 25);
			LocalDate date8 = LocalDate.of(2001, 4, 19);
			visit1.setDate(date1);
			visit2.setDate(date2);
			visit3.setDate(date3);
			visit4.setDate(date4);
			visit5.setDate(date5);
			visit6.setDate(date6);
			visit7.setDate(date7);
			visit8.setDate(date8);
			for (int i=0; i<8; i++){
				pet.addVisit(visits.get(i));
			}
		}

		@After
		public void tearDown() {
			pet = null;
		}

		public boolean checkDates(LocalDate start, LocalDate end, List<Visit> visitsBetween){
			//check output is correct
			for (Visit ovisit: visitsBetween){
				if (ovisit.getDate().isBefore(start) || ovisit.getDate().isAfter(end)){
					return false;
				}
			}
			//check output is complete
			for (Visit svisit: visits){
				if (visitsBetween.contains(svisit)){
					continue;
				}
				if(svisit.getDate().isBefore(end) && svisit.getDate().isAfter(start)){
					return false;
				}
			}
			return true;
		}

		@DataPoints
		public static LocalDate[] starts() {
			LocalDate date1 = LocalDate.of(2000, 10, 21);
			LocalDate date2 = LocalDate.of(2003, 2, 2);
			LocalDate date3 = LocalDate.of(2007, 8, 31);
			return new LocalDate[]{date1,date2,date3};
		}

		@DataPoints
		public static LocalDate[] ends() {
			LocalDate date1 = LocalDate.of(2004, 5, 29);
			LocalDate date2 = LocalDate.of(2010, 9, 26);
			LocalDate date3 = LocalDate.of(2020, 3, 16);
			return new LocalDate[]{date1, date2, date3};

		}

		@Theory
		public void getVisitsBetweenTest(LocalDate start, LocalDate end){
			//prune wrong inputs
			assumeTrue(start.isBefore(end));
			assumeTrue(!start.isEqual(end));

			List<Visit> visitsBetween = pet.getVisitsBetween(start, end);
			assumeTrue(checkDates(start, end, visitsBetween));
		}
	}

	@RunWith(Parameterized.class)
	public static class PartTwoParameterizedTest {
		private Pet dummy_pet;
		public int birthday;
		public int age;
		public List<Integer> all_visits;
		public List<Integer> expected_visits;

		public PartTwoParameterizedTest(int birthday, List<Integer> all_visits, int age, List<Integer> expected_visits){
			this.expected_visits = expected_visits;
			this.all_visits = all_visits;
			this.birthday = birthday;
			this.age = age;
		}

		@Before
		public void setup(){
			dummy_pet = new Pet();
		}

		@After
		public void after(){
			dummy_pet = null;
		}

		@Parameterized.Parameters
		public static Iterable<Object[]> data()
		{
			return Arrays.asList(new Object[][]{
				{0,Arrays.asList(1,2,3),2,Arrays.asList(1)},
				{0, Arrays.asList(1,3,5,6,7), 4, Arrays.asList(1,3)},
				{0, Arrays.asList(3,4,5,6,8), 6,Arrays.asList(3,4,5)}
			});
		}


		@Test
		public void test_getVisitsUntilAge(){
			dummy_pet.setBirthDate(LocalDate.of(birthday,1,1));
			List<Visit> result_visits = new ArrayList<>();

			for (int visit:all_visits){
				LocalDate test_date =  LocalDate.of(visit,1,1);
				Visit test_visit = new Visit();
				test_visit.setDate(test_date);
				dummy_pet.addVisit(test_visit);
				if (expected_visits.contains(visit)){
					result_visits.add(test_visit);
				}
			}
			assertEquals(dummy_pet.getVisitsUntilAge(age),result_visits);
		}
	}

	public static class PartOneTests{

		private Pet dummy_pet;
		private Owner dummy_owner;
		private Visit dummy_visit;

		@Before
		public void setup() {
			this.dummy_pet = new Pet();
			this.dummy_owner = new Owner();
			this.dummy_visit = new Visit();
		}
		@After
		public void tearDown(){
			this.dummy_pet = null;
			this.dummy_owner = null;
			this.dummy_visit = null;
		}
		private List<Visit> generate_visits_that_first_isExpected(int times){
			List<Visit> expected_visits = new ArrayList<>();
			for (int i = 0; i < times; i++){
				LocalDate visit_date =  LocalDate.of(1990 + (10*(i+1)),1,1);
				Visit new_visit = new Visit();
				new_visit.setDate(visit_date);
				dummy_pet.addVisit(new_visit);
				if (i == 0){expected_visits.add(new_visit);}
			}
			return expected_visits;
		}
		@Test
		public void test_getVisitsUntilAge(){
			assertTrue(dummy_pet.getVisits().isEmpty());
			List<Visit> expected_visits = new ArrayList<>();
			dummy_pet.setBirthDate(LocalDate.of(1990,1,1));
			expected_visits = generate_visits_that_first_isExpected(2);
			assertEquals(dummy_pet.getVisitsUntilAge(15),expected_visits);

		}
		@Test
		public void test_getVisitsBetweenTest(){
			assertTrue(dummy_pet.getVisits().isEmpty());
			List<Visit> expected_visits = new ArrayList<>();
			LocalDate first_visit = LocalDate.of(1990,1,1);
			LocalDate second_visit = LocalDate.of(2005,1,1);
			expected_visits = generate_visits_that_first_isExpected(2);
			assertEquals(dummy_pet.getVisitsBetween(first_visit, second_visit),expected_visits);

		}
		@Test
		public void test_setBirthDate(){
			LocalDate test_date =  LocalDate.now();
			dummy_pet.setBirthDate(test_date);
			assertEquals(test_date, dummy_pet.getBirthDate());
		}
		@Test
		public void test_getBirthDate(){
			LocalDate test_date = dummy_pet.getBirthDate();
			assertNull(test_date);
		}
		@Test
		public void test_getType(){
			PetType test_type = dummy_pet.getType();
			assertNull(test_type);
		}
		@Test
		public void test_setType(){
			PetType test_type = new PetType();
			test_type.setName("german shepherd");
			dummy_pet.setType(test_type);
			assertEquals(test_type, dummy_pet.getType());
		}
		@Test
		public void test_getOwner(){
			assertNull(dummy_pet.getOwner());

			dummy_pet.setOwner(dummy_owner);
			assertEquals(dummy_pet.getOwner(), dummy_owner);

		}
		@Test
		public void test_setOwner(){
			dummy_pet.setOwner(dummy_owner);
			assertEquals(dummy_pet.getOwner(), dummy_owner);

			Owner new_owner = new Owner();
			dummy_pet.setOwner(new_owner);
			assertEquals(dummy_pet.getOwner(), new_owner);
		}

		@Test
		public void test_setVisitsInternal(){
			List<Visit> visits = new ArrayList<>();
			visits.add(dummy_visit);
			dummy_pet.setVisitsInternal(visits);
			assertEquals(dummy_pet.getVisits(),visits);

		}

		@Test //this test expect NullPointerException
		public void test_getVisitsUntilAge_withNullVisit(){
			dummy_pet.addVisit(dummy_visit);
			dummy_visit = null;
			Throwable exception = assertThrows(NullPointerException.class, () -> dummy_pet.getVisitsUntilAge(0));

		}

		@Test
		public void test_removeVisit(){
			List<Visit> expected_visits = new ArrayList<>();
			Visit visit1 = new Visit();
			Visit visit2 = new Visit();

			expected_visits.add(visit1);
			dummy_pet.addVisit(visit1);
			expected_visits.add(visit2);
			dummy_pet.addVisit(visit2);
			assertEquals(dummy_pet.getVisits(), expected_visits);

			dummy_pet.removeVisit(visit1);
			expected_visits.remove(visit1);

			assertEquals(dummy_pet.getVisits(), expected_visits);

			dummy_pet.removeVisit(visit2);
			expected_visits.remove(visit2);
			assertEquals(dummy_pet.getVisits(), expected_visits);
		}
		@Test
		public void test_addVisit(){
			List<Visit> expected_visits = new ArrayList<>();
			assertTrue(dummy_pet.getVisits().isEmpty());
			dummy_pet.addVisit(dummy_visit);
			expected_visits.add(dummy_visit);
			assertEquals(dummy_pet.getVisits().size(),expected_visits.size());
			Visit new_dummy_visit = new Visit();
			dummy_pet.addVisit(new_dummy_visit);
			expected_visits.add(new_dummy_visit);
			assertEquals(dummy_pet.getVisits().size(),expected_visits.size());
			assertEquals(dummy_pet.getVisits(), expected_visits);

		}

		@Test //in this test we check if the output is sorted
		public void test_getVisits(){
			List<Visit> expected_visits = new ArrayList<>();
			assertTrue(dummy_pet.getVisits().isEmpty());
			Visit new_dummy_visit = new Visit();
			new_dummy_visit.setDate(LocalDate.of(0,1,1));
			dummy_pet.addVisit(new_dummy_visit);
			expected_visits.add(new_dummy_visit);
			new_dummy_visit = new Visit();
			new_dummy_visit.setDate(LocalDate.of(4,1,1));
			dummy_pet.addVisit(new_dummy_visit);
			expected_visits.add(new_dummy_visit);
			new_dummy_visit = new Visit();
			new_dummy_visit.setDate(LocalDate.of(6,1,1));
			dummy_pet.addVisit(new_dummy_visit);
			expected_visits.add(new_dummy_visit);
			PropertyComparator.sort(expected_visits, new MutableSortDefinition("date", false, false));
			assertEquals(dummy_pet.getVisits(),expected_visits);

		}
	}
}
